(function ($) {

  Drupal.behaviors.entityReferencePreview = {
    attach: function (context, settings) {
      $.each($('.entity-reference-preview-change-button:not(".entity-reference-preview-processed")'), function(index, value) {
        $(this).parent().prev().children('input.form-text').hide();
      });

      $('.entity-reference-preview-change-button', context).once('entity-reference-preview').click(function () {
        $(this).parent().hide().prev().children('input.form-text').fadeIn().val('').focus();
      });
    }
  };

})(jQuery);