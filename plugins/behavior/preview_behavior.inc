<?php

$plugin = array(
  'title' => t('Preview'),
  'class' => 'EntityReferenceInstanceBehaviorPreview',
  'weight' => 10,
  'behavior type' => 'instance',
);
