<?php

class EntityReferenceInstanceBehaviorPreview extends EntityReference_BehaviorHandler_Abstract {
  /**
   * Generate a settings form for this handler.
   */
  public function settingsForm($field, $instance) {
    if (isset($field['settings']['target_type'])) {
      $entity_info = entity_get_info($field['settings']['target_type']);
      // Add a none value.
      $view_modes['_none'] = t('-None-');
      foreach($entity_info['view modes'] as $view_mode_key => $view_mode_data) {
        $view_modes[$view_mode_key] = $view_mode_data['label'];
      }

      $form['selected_view_mode'] = array(
        '#title' => t('View mode for rendering the selected item.'),
        '#type' => 'select',
        '#options' => $view_modes,
      );

      $form['autocomplete_view_mode'] = array(
        '#title' => t('View mode for rendering the autocomplete items.'),
        '#type' => 'select',
        '#options' => $view_modes,
      );
    }
    else {
      drupal_set_message(t('Please choose an entity type first.'), 'status');
    }

    return $form;
  }
}
